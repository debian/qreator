# -*- Mode: Python; coding: utf-8; indent-tabs-mode: nil; tab-width: 4 -*-
### BEGIN LICENSE
# Copyright (C) 2012 David Planella <david.planella@ubuntu.com>
# This program is free software: you can redistribute it and/or modify it 
# under the terms of the GNU General Public License version 3, as published 
# by the Free Software Foundation.
# 
# This program is distributed in the hope that it will be useful, but 
# WITHOUT ANY WARRANTY; without even the implied warranties of 
# MERCHANTABILITY, SATISFACTORY QUALITY, or FITNESS FOR A PARTICULAR 
# PURPOSE.  See the GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License along 
# with this program.  If not, see <http://www.gnu.org/licenses/>.
### END LICENSE

import os.path
import optparse

from qreator_lib.i18n import _
from gi.repository import Gtk, Gdk, GLib, GObject  # pylint: disable=E0611
from qreator import QreatorWindow
from qreator_lib import set_up_logging, get_version, get_data_path, VendorError
from qreator.qrcodes.QRCodeType import QRCodeType
import logging

# use dynamic importing to get all qrcode dataformats without
# knowing their names.
# That way, all the information of a qrcode dataformat can
# be kept in a single file, like a plugin.
import pkgutil
import qreator.qrcodes

plugin_warnings = []
for qr in list(pkgutil.iter_modules(qreator.qrcodes.__path__)):
    name = "qreator.qrcodes." + qr[1]
    try:
        __import__(name, fromlist=[])
    except VendorError as e:
        plugin_warnings.append("%s: %s." % (name, e))


def UTF8_(message):
    return _(message).decode('UTF-8')


def parse_options(qr_types):
    """Support for command line options"""
    parser = optparse.OptionParser(version="%%prog %s" % get_version())
    parser.add_option(
        "-v", "--verbose", action="count", dest="verbose",
        help=UTF8_("Show debug messages (-vv debugs qreator_lib also)"))

    for n, qr in enumerate(qr_types):
        parser.add_option(
            qr.cmdline_option_short, qr.cmdline_option_long,
            dest="view", action="store_const", const=n,
            help=qr.message.decode('UTF-8'))

    (options, args) = parser.parse_args()

    set_up_logging(options)
    return options


def main():
    'constructor for your class instances'
    # Fix icons which are missing in 12.04
    icontheme = Gtk.IconTheme.get_default()
    icontheme.append_search_path(os.path.join(get_data_path(), "media"))

    # Run the application
    window = QreatorWindow.QreatorWindow()

    qr_types = [d(window.update_qr_code) for d in QRCodeType.dataformats]
    options = parse_options(qr_types)

    logger = logging.getLogger('qrcodes')
    for plugin_warning in plugin_warnings:
        logger.warn(plugin_warning)
    
    window.qr_types = qr_types
    window.show()
    if getattr(options, 'view', None) is not None:
        qr_id = options.view
        window.switch_qrcode_view(qr_id)
        window.ui.notebook1.set_current_page(QreatorWindow.PAGE_QR)
    GObject.threads_init()
    GLib.threads_init()
    Gdk.threads_init()
    Gtk.main()
