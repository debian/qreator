# -*- Mode: Python; coding: utf-8; indent-tabs-mode: nil; tab-width: 4 -*-
### BEGIN LICENSE
# Copyright (C) 2012 David Planella <david.planella@ubuntu.com>
# This program is free software: you can redistribute it and/or modify it 
# under the terms of the GNU General Public License version 3, as published 
# by the Free Software Foundation.
# 
# This program is distributed in the hope that it will be useful, but 
# WITHOUT ANY WARRANTY; without even the implied warranties of 
# MERCHANTABILITY, SATISFACTORY QUALITY, or FITNESS FOR A PARTICULAR 
# PURPOSE.  See the GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License along 
# with this program.  If not, see <http://www.gnu.org/licenses/>.
### END LICENSE

from gi.repository import Gtk  # pylint: disable=E0611


def clear_text_entry(widget, icon):
    if icon == Gtk.EntryIconPosition.SECONDARY:
        widget.set_text("")
        show_clear_icon(widget)
    elif icon == Gtk.EntryIconPosition.PRIMARY:
        widget.select_region(0, -1)
        widget.grab_focus()


def show_clear_icon(widget):
    """
    Show the clear icon whenever the field is not empty
    """
    if widget.get_text() != "":
        widget.set_icon_from_stock(Gtk.EntryIconPosition.SECONDARY,
            Gtk.STOCK_CLEAR)
        # Reverse the icon if we are using an RTL locale
        if widget.get_direction() == Gtk.TextDirection.RTL:
            pb = widget.get_icon_pixbuf(
                Gtk.EntryIconPosition.SECONDARY).flip(True)
            widget.set_icon_from_pixbuf(Gtk.EntryIconPosition.SECONDARY, pb)
    else:
        widget.set_icon_from_stock(Gtk.EntryIconPosition.SECONDARY, None)
