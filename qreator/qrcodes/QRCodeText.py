# -*- Mode: Python; coding: utf-8; indent-tabs-mode: nil; tab-width: 4 -*-
### BEGIN LICENSE
# Copyright (C) 2012 David Planella <david.planella@ubuntu.com>
# This program is free software: you can redistribute it and/or modify it 
# under the terms of the GNU General Public License version 3, as published 
# by the Free Software Foundation.
# 
# This program is distributed in the hope that it will be useful, but 
# WITHOUT ANY WARRANTY; without even the implied warranties of 
# MERCHANTABILITY, SATISFACTORY QUALITY, or FITNESS FOR A PARTICULAR 
# PURPOSE.  See the GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License along 
# with this program.  If not, see <http://www.gnu.org/licenses/>.
### END LICENSE

from qreator_lib.i18n import _
from . QRCodeType import QRCodeType
from QRCodeTextGtk import QRCodeTextGtk

class QRCodeText(QRCodeType):
    description =  _('Text')
    icon_name = 'text.png'
    cmdline_option_long = "--text"
    cmdline_option_short = "-t"
    message = _("Create a QR code from text")

    def create_widget(self):
        self.widget = QRCodeTextGtk(self.qr_code_update_func)
