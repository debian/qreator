# -*- Mode: Python; coding: utf-8; indent-tabs-mode: nil; tab-width: 4 -*-
### BEGIN LICENSE
# Copyright (C) 2012 David Planella <david.planella@ubuntu.com>
# This program is free software: you can redistribute it and/or modify it 
# under the terms of the GNU General Public License version 3, as published 
# by the Free Software Foundation.
# 
# This program is distributed in the hope that it will be useful, but 
# WITHOUT ANY WARRANTY; without even the implied warranties of 
# MERCHANTABILITY, SATISFACTORY QUALITY, or FITNESS FOR A PARTICULAR 
# PURPOSE.  See the GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License along 
# with this program.  If not, see <http://www.gnu.org/licenses/>.
### END LICENSE

from gi.repository import Gtk
from qreator_lib.helpers import get_data_file


class QRCodeTextGtk(object):
    def __init__(self, qr_code_update_func):
        self.qr_code_update_func = qr_code_update_func
        builder = Gtk.Builder()

        builder.add_from_file(
            get_data_file('ui', '%s.ui' % ('QrCodeText',)))
        self.grid = builder.get_object('qr_code_text')

        textbuffer = builder.get_object('entryText').get_buffer()
        textbuffer.connect("changed", self.on_entryText_changed, None)

    def on_activated(self):
        pass

    def on_prepared(self):
        pass

    def on_entryText_changed(self, widget, data=None):
        self.qr_code_update_func(widget.get_text(widget.get_start_iter(),
                                            widget.get_end_iter(),
                                            False))
