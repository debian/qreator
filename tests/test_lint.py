#!/usr/bin/python
# -*- Mode: Python; coding: utf-8; indent-tabs-mode: nil; tab-width: 4 -*-
### BEGIN LICENSE
# Copyright (C) 2012 David Planella <david.planella@ubuntu.com>
# This program is free software: you can redistribute it and/or modify it 
# under the terms of the GNU General Public License version 3, as published 
# by the Free Software Foundation.
# 
# This program is distributed in the hope that it will be useful, but 
# WITHOUT ANY WARRANTY; without even the implied warranties of 
# MERCHANTABILITY, SATISFACTORY QUALITY, or FITNESS FOR A PARTICULAR 
# PURPOSE.  See the GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License along 
# with this program.  If not, see <http://www.gnu.org/licenses/>.
### END LICENSE

import unittest
import subprocess


class TestPylint(unittest.TestCase):

    # un-comment the following for loads of diagnostics
    def test_project_full_report(self):
        '''Only for the brave
        you will have to make judgement calls about your code standards
        that differ from the norm'''
        #subprocess.call(["pylint", '-E', 'qreator'])
        subprocess.call(["pylint", 'qreator', 'qreator_lib'])

if __name__ == '__main__':
    # you will get better results with nosetests
    import nose
    nose.main()
